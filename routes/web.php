<?php

/**
 * Authentication
 */
Route::get('login', 'Auth\AuthController@getLogin')->name('login');
Route::post('login', 'Auth\AuthController@postLogin')->name('post.login');

Route::get('logout', [
    'as' => 'auth.logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

Route::group(['middleware' => 'auth'], function () {

    /**
     * Dashboard
     */
    Route::get('/', [
        'as' => 'dashboard',
        'uses' => 'DashboardController@index'
    ]);

    /**
     * Profiles Details
     */
    Route::get('/profile', [
        'as' => 'profiles.index',
        'uses' => 'ProfileController@index'
    ]);
    Route::post('/profile/profile', [
        'as' => 'profiles.profile',
        'uses' => 'ProfileController@profileUpdate'
    ]);
    Route::post('/profile/account', [
        'as' => 'profiles.account',
        'uses' => 'ProfileController@accountUpdate'
    ]);
    Route::post('/profile/password', [
        'as' => 'profiles.password',
        'uses' => 'ProfileController@passwordUpdate'
    ]);

    /*
    * Users, Role, and Permissions
    */
    Route::resource('users','UserController');
    Route::resource('roles','RoleController');
    Route::resource('permissions','PermissionsController');

    /*
     * Laporan
     */
    Route::get('laporan-absensi/{tipe}', [
       'as' => 'laporan-absensi.tipe',
       'uses' => 'LaporanController@indexLaporan',
       'middleware' => 'permission:laporan-manage'
    ]);
    Route::get('laporan-absensi/get-unit/{opd_id}', [
        'as' => 'get.ajax-unit',
        'uses' => 'LaporanController@ajaxUnit'
    ]);
    Route::get('laporan-absensi/get-pegawai/{opd_id}', [
        'as' => 'get.ajax-pegawai',
        'uses' => 'LaporanController@ajaxPegawai'
    ]);
    Route::post('laporan-absensi/tipe-laporan/{tipe}', [
        'as' => 'post.tipe-laporan',
        'uses' => 'LaporanController@getTipeDataLaporan'
    ]);
    Route::put('laporan-absensi/tipe-laporan/{tipe}', [
        'as' => 'cetak.tipe-laporan',
        'uses' => 'LaporanController@pdfPrint'
    ]);
});