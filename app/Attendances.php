<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendances extends Model
{
    protected $connection = 'mysql';

    protected $table = 'attendances';
    
    public $timestamps = false;

    public function opd()
    {
        return $this->belongsTo(MOpd::class);
    }

    public function unit_kerja()
    {
        return $this->belongsTo(MUnitKerja::class);
    }

    public function pegawai()
    {
        return $this->belongsTo(MPegawai::class, 'pegawai_id');
    }
}
