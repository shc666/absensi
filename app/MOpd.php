<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MOpd extends Model
{
    protected $connection = 'simpeg';
    
    protected $table = 'm_opd';

    public $timestamps = false;

    protected $guarded = [];

    public function attendances()
    {
        return $this->hasMany(Attendances::class);
    }
}