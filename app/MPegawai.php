<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MPegawai extends Model
{
    protected $connection = 'simpeg';
    
    protected $table = 'm_pegawai';

    public $timestamps = false;

    protected $guarded = [];

    public function attendances()
    {
        return $this->hasMany(Attendances::class);
    }
}