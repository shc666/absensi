<?php

namespace App\Helpers;
use Carbon\Carbon;

class Format
{
    static private $monthShort = [
        1 => "Jan",
        2 => "Feb",
        3 => "Mar",
        4 => "Apr",
        5 => "Mei",
        6 => "Jun",
        7 => "Jul",
        8 => "Agt",
        9 => "Sep",
        10 => "Okt",
        11 => "Nop",
        12 => "Des"
    ];

    static private $monthLong = [
        1 => "Januari",
        2 => "Februari",
        3 => "Maret",
        4 => "April",
        5 => "Mei",
        6 => "Juni",
        7 => "Juli",
        8 => "Agustus",
        9 => "September",
        10 => "Oktober",
        11 => "Nopember",
        12 => "Desember"
    ];

    static private $day = [
        1	=> "Minggu",
        2	=> "Senin",
        3	=> "Selasa",
        4	=> "Rabu",
        5	=> "Kamis",
        6	=> "Jumat",
        7	=> "Sabtu"
    ];

    static public function getDayByFormat($date, $format="Y-m-d H:i:s")
    {
        $num = Carbon::createFromFormat("Y-m-d", $date)->dayOfWeek+1;
        return self::$day[$num];
    }
    static public function getDayDateByFormat($date, $format="Y-m-d H:i:s")
    {
        $date = Carbon::createFromFormat("Y-m-d", $date);
        return self::$day[$date->dayOfWeek+1].', '.$date->format(config("absensi.formatdate"));
    }
    static public function datetime($time, $format="Y-m-d H:i:s")
    {
        return Carbon::createFromFormat($format, $time)->format(config("absensi.formatdatetime"));
    }
    static public function date($date, $format="Y-m-d H:i:s")
    {
        return Carbon::createFromFormat($format, $date)->format(config("absensi.formatdate"));
    }
    static public function time($date, $format="Y-m-d H:i:s")
    {
        return Carbon::createFromFormat($format, $date)->format("H:i:s");
    }
    static public function puredate($time, $format="Y-m-d H:i:s")
    {
        return Carbon::createFromFormat($format, $time);
    }

    static public function getMonthShort($month = null)
    {
        return is_null($month)?self::$monthShort:self::$monthShort[$month];
    }

    static public function getMonthLong($month = null)
    {
        return is_null($month)?self::$monthLong:self::$monthLong[$month];
    }

    static public function zeroPad($input, $length=2)
    {
        return str_pad($input, $length, '0', STR_PAD_LEFT);
    }

    static public function currency($value)
    {
        return number_format($value, 2, ",", ".");
    }
}