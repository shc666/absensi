<?php 

namespace App\Helpers;
   
class IndonesianTime
{
    public static function toIndonesia($tgl) 
    {
        $dt = new \Carbon\Carbon($tgl);
        setlocale(LC_ALL, 'id_ID', 'id_ID.utf8', 'IND', 'Indonesia', 'Indonesian');
            
        return $dt->formatLocalized('%A, %e %B %Y');
    }

    public static function toIndonesia2($tgl) 
    {
        $dt = new \Carbon\Carbon($tgl);
        setlocale(LC_ALL, 'id_ID', 'id_ID.utf8', 'IND', 'Indonesia', 'Indonesian');
            
        return $dt->formatLocalized('%e %B %Y');
    }

    public static function toIndonesia_w_time($tgl)
    {
        $dt = new \Carbon\Carbon($tgl);
        setlocale(LC_ALL, 'id_ID', 'id_ID.utf8', 'IND', 'Indonesia', 'Indonesian');
            
        return $dt->formatLocalized('%A, %e %B %Y %H:%M:%S');
    }

    public static function toIndonesiaMonthYear($tgl)
    {
        $dt = new \Carbon\Carbon($tgl);
        setlocale(LC_ALL, 'id_ID', 'id_ID.utf8', 'IND', 'Indonesia', 'Indonesian');
            
        return $dt->formatLocalized('%B %Y');
    }
}