<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingDays extends Model 
{
    protected $connection = 'mysql';

    protected $table = 'working_days';

	protected $fillable = [
		'updated_by', 'day', 'working_status',
	];
}