<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Laporan\LaporanRequest;
use App\Attendances;
use App\MOpd;
use App\MUnitKerja;
use App\MPegawai;
use App\WorkingDays;
use App\Helpers\IndonesianTime;
use Carbon\Carbon;
use DB;
use PDF;

class LaporanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:laporan-manage');
    }

    /*
     * Function Get Type Reports 
     */
    public function getTipeLaporan($int)
    {
        $types = [
            1 => "Kehadiran",
            2 => "Individu",
            3 => "Rekap",
            4 => "Uang Makan"
        ];
        foreach($types as $key => $value)
        {
            if($key == $int)
            {
                return $value;
            }
        }
        
        return abort(404);
    }

    /**
     * Index Tipe Laporan Absensi.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexLaporan($tipe)
    {
        $header = $this->getTipeLaporan($tipe);

        $month_name = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $months = [];
        for($i = 1; $i < 13; $i++)
		{
            $months[] = [
                'value' => $i,
                'text'  => $month_name[$i]
            ];
        }

        $years = Attendances::select(DB::raw('year(date) as tahun'))
                ->orderBy(DB::raw('year(date)'))
                ->groupBy(DB::raw('year(date)'))
                ->get();

        // $opds = MOpd::select('id', 'nama_opd')->get();
        $opds = MOpd::select('id', 'nama_opd')
                ->whereIn('id', [1,9])
                ->get();
        
        return view('laporan.index', compact('header', 'months', 'years', 'opds', 'tipe'));
    }

    /**
     * Ajax Unit Instance.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxUnit($opd_id)
    {
        $unit = MUnitKerja::select('id', 'unit_kerja')
                ->where('opd_id', $opd_id)
                ->get();
        
        return view('ajax.ajax-unit', compact('unit'));
    }

    /**
     * Ajax Pegawai Instance.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxPegawai($opd_id)
    {
        $employees = MPegawai::join('m_riwayat_jabatan as mrj', 'mrj.pegawai_id', 'm_pegawai.id')
                    ->where('mrj.opd_id', $opd_id)
                    ->select('mrj.pegawai_id', 'm_pegawai.nip', 'm_pegawai.nama')
                    ->groupBy('mrj.pegawai_id')
                    ->get();
        
        return view('ajax.ajax-pegawai', compact('employees'));
    }

    /**
     * Post Tipe Laporan.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTipeDataLaporan(LaporanRequest $request)
    {
        $tipe              = $request->tipe;
        $selected_month    = $request->select_month; 
        $selected_year     = $request->select_year;
        $selected_opd      = $request->select_opd;
        $selected_unit     = $request->select_unit;
        $selected_employee = $request->select_employee;
        $days = Carbon::createFromFormat("Y-m-d", $selected_year."-".$selected_month."-1")->endOfMonth()->format("j");
        $day_value = (int)$days;
        $date = $selected_year."-".$selected_month;

        $query_emp = MPegawai::select([
                    'mrj.pegawai_id as id_pegawai',
                    DB::raw('group_concat(DISTINCT m_pegawai.nip, " / ", m_pegawai.nama) as pegawai')
                ])
                ->join('m_riwayat_jabatan as mrj', 'mrj.pegawai_id', '=', 'm_pegawai.id')
                ->groupBy('mrj.pegawai_id')
                ->orderBy('mrj.pegawai_id', 'ASC');

        $query_att = Attendances::select([
                    'attendances.pegawai_id as id_pegawai','attendances.date as tanggal', 'attendances.in as absen_in', 'attendances.out as absen_out'
                ])
                ->groupBy('attendances.id');

        $query_hol = WorkingDays::where('working_status', 0);

        $query_cou = Attendances::select([
                    'attendances.pegawai_id as id_pegawai',
                    DB::raw('count(attendances.pegawai_id) as total_absen')
                ])
                ->groupBy('attendances.pegawai_id');

        if($tipe == 1)
        {
            if($selected_unit != '')
            {
                $employees = $query_emp->where([
                                'mrj.opd_id'        => $selected_opd,
                                'mrj.unit_kerja_id' => $selected_unit
                            ])
                            ->get()
                            ->toArray();

                $attendances = $query_att->whereMonth('date', $selected_month)
                                ->whereYear('date', $selected_year)
                                ->where([
                                    'attendances.opd_id'        => $selected_opd,
                                    'attendances.unit_kerja_id' => $selected_unit
                                ])
                                ->get()
                                ->toArray();

                $weekly_holidays = $query_hol->get()->toArray();

                $counts = $query_cou->whereMonth('date', $selected_month)
                        ->whereYear('date', $selected_year)
                        ->where([
                            'attendances.opd_id'        => $selected_opd,
                            'attendances.unit_kerja_id' => $selected_unit
                        ])
                        ->get()
                        ->toArray();
            }
            else
            {
                $employees = $query_emp->where([
                                'mrj.opd_id' => $selected_opd
                            ])
                            ->get()
                            ->toArray();

                $attendances = $query_att->whereMonth('date', $selected_month)
                                ->whereYear('date', $selected_year)
                                ->where([
                                    'attendances.opd_id' => $selected_opd
                                ])
                                ->get()
                                ->toArray();

                $weekly_holidays = $query_hol->get()->toArray();

                $counts = $query_cou->whereMonth('date', $selected_month)
                        ->whereYear('date', $selected_year)
                        ->where([
                            'attendances.opd_id' => $selected_opd
                        ])
                        ->get()
                        ->toArray();
            }
            
            return view('laporan.kehadiran', compact('attendances', 'counts', 'date', 'day_value', 'employees', 'tipe', 'selected_month', 'selected_year', 'selected_opd', 'selected_unit', 'weekly_holidays'));
        }
        elseif($tipe == 2)
        {
            $employees = $query_emp->where([
                            'mrj.opd_id'     => $selected_opd,
                            'mrj.pegawai_id' => $selected_employee
                        ])
                        ->get()
                        ->toArray();

            $attendances = $query_att->whereMonth('date', $selected_month)
                            ->whereYear('date', $selected_year)
                            ->where([
                                'attendances.opd_id'     => $selected_opd,
                                'attendances.pegawai_id' => $selected_employee
                            ])
                            ->get()
                            ->toArray();

            $weekly_holidays = $query_hol->get()->toArray();

            $counts = $query_cou->whereMonth('date', $selected_month)
                    ->whereYear('date', $selected_year)
                    ->where([
                        'attendances.opd_id'        => $selected_opd,
                        'attendances.pegawai_id'    => $selected_employee
                    ])
                    ->get()
                    ->toArray();

            // return response()->json([
            //     'selected_month' => $selected_month,
            //     'selected_year'  => $selected_year,
            //     'selected_opd'   => $selected_opd,
            //     'selected_employee' => $selected_employee,
            //     'employee'       => $employees,
            //     'attendances'    => $attendances,
            //     'holidays'       => $weekly_holidays,
            //     'counts'         => $counts,
            //     'days'           => $day_value
            // ]);
            
            return view('laporan.individu', compact('attendances', 'counts', 'date', 'day_value', 'employees', 'tipe', 'selected_month', 'selected_year', 'selected_opd', 'selected_employee', 'weekly_holidays'));
        }
        elseif($tipe == 3)
        {
            // Rekap
            return view('laporan.rekap');
        }
        else
        {
            // Uang Makan
            return view('laporan.uang-makan');
        }
    }

    /**
     * Print PDF Tipe Laporan.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfPrint(Request $request)
    {
        /* Set size limit generate PDF */
        ini_set("pcre.backtrack_limit", "50000000");
        ini_set("pcre.recursion_limit", "50000000");

        $tipe              = $request->tipe;
        $selected_month    = $request->selected_month; 
        $selected_year     = $request->selected_year;
        $selected_opd      = $request->selected_opd;
        $selected_unit     = $request->selected_unit;
        $selected_employee = $request->selected_employee;
        $days = Carbon::createFromFormat("Y-m-d", $selected_year."-".$selected_month."-1")->endOfMonth()->format("j");
        $day_value = (int)$days;
        $date = $selected_year."-".$selected_month;
        $indonesian_format = IndonesianTime::toIndonesiaMonthYear(Carbon::parse($date));
        $opd_name = MOpd::where('id', $selected_opd)->pluck('nama_opd')->first();
        $unit_name = MUnitKerja::where('id', $selected_unit)->pluck('unit_kerja')->first();

        $query_emp = MPegawai::select([
                    'mrj.pegawai_id as id_pegawai',
                    DB::raw('group_concat(DISTINCT m_pegawai.nip, " / ", m_pegawai.nama) as pegawai')
                ])
                ->join('m_riwayat_jabatan as mrj', 'mrj.pegawai_id', '=', 'm_pegawai.id')
                ->groupBy('mrj.pegawai_id')
                ->orderBy('mrj.pegawai_id', 'ASC');

        $query_att = Attendances::select([
                    'attendances.pegawai_id as id_pegawai','attendances.date as tanggal', 'attendances.in as absen_in', 'attendances.out as absen_out'
                ])
                ->groupBy('attendances.id');

        $query_hol = WorkingDays::where('working_status', 0);

        $query_cou = Attendances::select([
                    'attendances.pegawai_id as id_pegawai',
                    DB::raw('count(attendances.pegawai_id) as total_absen')
                ])
                ->groupBy('attendances.pegawai_id');

        if($tipe == 1)
        {
            if($selected_unit != '')
            {
                $employees = $query_emp->where([
                                'mrj.opd_id'        => $selected_opd,
                                'mrj.unit_kerja_id' => $selected_unit
                            ])
                            ->get()
                            ->toArray();

                $attendances = $query_att->whereMonth('date', $selected_month)
                                ->whereYear('date', $selected_year)
                                ->where([
                                    'attendances.opd_id'        => $selected_opd,
                                    'attendances.unit_kerja_id' => $selected_unit
                                ])
                                ->get()
                                ->toArray();
                
                $weekly_holidays = $query_hol->get()->toArray();

                $counts = $query_cou->whereMonth('date', $selected_month)
                        ->whereYear('date', $selected_year)
                        ->where([
                            'attendances.opd_id'        => $selected_opd,
                            'attendances.unit_kerja_id' => $selected_unit
                        ])
                        ->get()
                        ->toArray();
            }
            else
            {
                $employees = $query_emp->where([
                                'mrj.opd_id' => $selected_opd
                            ])
                            ->get()
                            ->toArray();

                $attendances = $query_att->whereMonth('date', $selected_month)
                                ->whereYear('date', $selected_year)
                                ->where([
                                    'attendances.opd_id' => $selected_opd
                                ])
                                ->get()
                                ->toArray();

                $weekly_holidays = $query_hol->get()->toArray();

                $counts = $query_cou->whereMonth('date', $selected_month)
                        ->whereYear('date', $selected_year)
                        ->where([
                            'attendances.opd_id' => $selected_opd
                        ])
                        ->get()
                        ->toArray();
            }

            $pdf = PDF::loadView('laporan.cetak-kehadiran', compact('date', 'day_value', 'employees', 'attendances', 'counts', 'indonesian_format', 'opd_name', 'unit_name', 'weekly_holidays'), [], [
                'default-font'      => 'Times New Roman',
                'title'             => 'LAPORAN ABSENSI KEHADIRAN BULAN '.strtoupper($indonesian_format),
                'watermark'         => 'S I S T E M - A B S E N S I',
                'show_watermark'    => true,
                'showImageErrors'   => true,
                'orientation'       => 'L',
            ]);

            return $pdf->stream('Laporan Absensi Kehadiran Bulan '.$indonesian_format.'.pdf');
        }
        elseif($tipe == 2)
        {
            $employees = MPegawai::select([
                'mrj.pegawai_id as id_pegawai',
                'mo.nama_opd as opd',
                'mu.unit_kerja as unit',
                DB::raw('group_concat(DISTINCT m_pegawai.nip, " / ", m_pegawai.nama) as pegawai')
            ])
            ->join('m_riwayat_jabatan as mrj', 'mrj.pegawai_id', '=', 'm_pegawai.id')
            ->join('m_opd as mo', 'mo.id', '=', 'mrj.opd_id')
            ->join('m_unit_kerja as mu', 'mu.id', '=', 'mrj.unit_kerja_id')
            ->groupBy('mrj.id')
            ->orderBy('mrj.pegawai_id', 'ASC')
            ->where([
                'mrj.opd_id'     => $selected_opd,
                'mrj.pegawai_id' => $selected_employee
            ])
            ->first();

            $attendances = $query_att->whereMonth('date', $selected_month)
                            ->whereYear('date', $selected_year)
                            ->where([
                                'attendances.opd_id'     => $selected_opd,
                                'attendances.pegawai_id' => $selected_employee
                            ])
                            ->get()
                            ->toArray();

            $weekly_holidays = $query_hol->get()->toArray();

            $counts = $query_cou->whereMonth('date', $selected_month)
                    ->whereYear('date', $selected_year)
                    ->where([
                        'attendances.opd_id'        => $selected_opd,
                        'attendances.pegawai_id'    => $selected_employee
                    ])
                    ->first();

            $pdf = PDF::loadView('laporan.cetak-individu', compact('attendances', 'counts', 'date', 'day_value', 'employees', 'indonesian_format', 'opd_name', 'selected_month', 'selected_year', 'weekly_holidays'), [], [
                'default-font'      => 'Times New Roman',
                'title'             => 'LAPORAN ABSENSI INDIVIDU BULAN '.strtoupper($indonesian_format),
                'watermark'         => 'S I S T E M - A B S E N S I',
                'show_watermark'    => true,
                'showImageErrors'   => true,
                'orientation'       => 'L',
            ]);

            return $pdf->stream('Laporan Absensi Individu Bulan '.$indonesian_format.'.pdf');
        }
        elseif($tipe == 3)
        {
            // Rekap
            
        }
        else
        {
            // Uang Makan

        }
    }
}