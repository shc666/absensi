<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Events\User\LoggedIn;
use Illuminate\Http\Request;
use Auth;
use Toastr;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout']]);
        $this->middleware('auth', ['only' => ['getLogout']]);
    }

     /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|AuthController
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function postLogin(LoginRequest $request)
    {
        $credentials = $request->getCredentials();

        if (Auth::attempt($credentials, $request->has('remember')))
        {
            return $this->handleUserWasAuthenticated($request);
        }

        // return redirect()->to('login')
        //         ->withErrors(trans('auth.failed'));
        return response()->json([
            "success" => false
        ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  Request $request
     * @param  bool $throttles
     * @param $user
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request)
    {
        if (method_exists($this, 'authenticated'))
        {
            return $this->authenticated($request, Auth::user());
        }

        event(new LoggedIn);

        Toastr::success('Login Berhasil!');

        return redirect()->intended();
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Auth::logout();

        Toastr::info('Logout Berhasil!');

        return redirect('login');
    }
}