<?php

namespace App\Http\Requests\Arsip\Lebih;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_perjanjian'     => 'required|unique:all_arsip,no_perjanjian',
            'tgl_entry'         => 'required',
            'uraian'            => 'required',
            'tgl_kesepakatan'   => 'required',
            'kategori_id'       => 'required',
            'files.*'           => 'sometimes|mimes:jpeg,jpg,png,pdf|max:5000',
        ];
    }

    public function messages()
    {
        return [
            'no_perjanjian.required'  => 'Bagian nomor perjanjian dibutuhkan',
            'no_perjanjian.unique'    => 'Nomor perjanjian sudah terisi dengan nilai yang sama. Silahkan masukan input yang berbeda',
            'files.mimes'             => 'Upload file dokumen harus berupa format (.jpg, .jpeg, .png, .pdf)',
            'files.max'               => 'Upload file dokumen hanya diperbolehkan dengan ukuran maksimal 5 Mb',
        ];
    }
}