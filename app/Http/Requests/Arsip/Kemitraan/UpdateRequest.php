<?php

namespace App\Http\Requests\Arsip\Kemitraan;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_perjanjian'     => 'required',
            'tgl_entry'         => 'required',
            'uraian'            => 'required',
            'tgl_kesepakatan'   => 'required',
            'kategori_id'       => 'required',
            'reuploadfiles.*'   => 'sometimes|mimes:jpeg,jpg,png,pdf|max:5000',
        ];
    }

    public function messages()
    {
        return [
            'reuploadfiles.mimes'   => 'Upload file dokumen harus berupa format (.jpg, .jpeg, .png, .pdf)',
            'reuploadfiles.max'     => 'Upload file dokumen hanya diperbolehkan dengan ukuran maksimal 5 Mb',
        ];
    }
}