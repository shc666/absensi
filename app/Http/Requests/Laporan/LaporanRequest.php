<?php

namespace App\Http\Requests\Laporan;

use App\Http\Requests\Request;

class LaporanRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'select_month' => 'required',
            'select_year'  => 'required',
            'select_opd'   => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'select_month.required' => 'Bagian pilihan bulan tidak boleh kosong',
            'select_year.required'  => 'Bagian pilihan tahun tidak boleh kosong',
            'select_opd.required'   => 'Bagian pilihan instansi tidak boleh kosong'
        ];
    }
}