<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MUnitKerja extends Model
{
    protected $connection = 'simpeg';
    
    protected $table = 'm_unit_kerja';

    public $timestamps = false;

    protected $guarded = [];

    public function attendances()
    {
        return $this->hasMany(Attendances::class);
    }
}