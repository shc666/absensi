window.Vue = require('vue');

/** 
 * Main Structure Layouts
*/
import Landing from './components/Landingpage/Landing.vue'

/**
 * Vue Router Callback And Global Plugins
 */
import Vue from 'vue'
// import Router from './routes.js'

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px',
  thickness: '3px',
  transition: {
    speed: '0.5s',
    opacity: '0.6s',
    termination: 300
  },
})


/**
 * Vue Render Init
 */
new Vue({
  el: '#app',
//   router: Router,
  components: {
    Landing
  },
});
