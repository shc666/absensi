<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Bagian :attribute harus disetujui.',
    'active_url'           => 'Bagian :attribute bukan alamt URL yang benar.',
    'after'                => 'Bagian :attribute harus diantara tanggal sebelum :date.',
    'alpha'                => 'Bagian :attribute harus berupa huruf.',
    'alpha_dash'           => 'Bagian :attribute harus hanya berupa huruf, angka, dan titik.',
    'alpha_num'            => 'Bagian :attribute harus hanya berupa huruf dan angka.',
    'array'                => 'Bagian :attribute harus berupa array.',
    'before'               => 'Bagian :attribute harus diantara tanggal sebelum :date.',
    'between'              => [
        'numeric' => 'Bagian :attribute harus diantara :min dan :max.',
        'file'    => 'Bagian :attribute harus harus diantara :min dan :max kilobytes.',
        'string'  => 'Bagian :attribute harus berada diantara :min dan :max karakter.',
        'array'   => 'Bagian :attribute harus memiliki barang antara :min dan :max.',
    ],
    'boolean'              => 'Bagian :attribute harus benar atau salah.',
    'confirmed'            => 'Bagian konfirmasi :attribute tidak sama.',
    'date'                 => 'Bagian :attribute bukan tanggal yang valid.',
    'date_format'          => 'Bagian :attribute tidak sesuai dengan format :format.',
    'different'            => 'Bagian :attribute dan :other harus berbeda.',
    'digits'               => 'Bagian :attribute harus :digits angka.',
    'digits_between'       => 'Bagian :attribute harus antara :min dan :max angka.',
    'email'                => 'Bagian :attribute harus alamat email yang valid.',
    'exists'               => 'Bagian terpilih :attribute tidak valid.',
    'filled'               => 'Bagian :attribute dibutuhkan.',
    'image'                => 'Bagian :attribute harus berupa gambar.',
    'in'                   => 'Bagian terpilih :attribute tidak valid.',
    'integer'              => 'Bagian :attribute harus sebuah integer.',
    'ip'                   => 'Bagian :attribute harus alamat IP yang valid.',
    'json'                 => 'Bagian :attribute harus berupa string JSON.',
    'max'                  => [
        'numeric' => 'Bagian :attribute tidak boleh lebih dari :max.',
        'file'    => 'Bagian :attribute may not be greater than :max kilobytes.',
        'string'  => 'Bagian :attribute may not be greater than :max characters.',
        'array'   => 'Bagian :attribute may not have more than :max items.',
    ],
    'mimes'                => 'Bagian :attribute harus berupa file dengan tipe: :values.',
    'min'                  => [
        'numeric' => 'Bagian :attribute harus setidaknya :min.',
        'file'    => 'Bagian :attribute harus setidaknya :min kilobytes.',
        'string'  => 'Bagian :attribute harus setidaknya :min karakter.',
        'array'   => 'Bagian :attribute harus mempunyai setidaknya :min.',
    ],
    'not_in'               => 'Bagian terpilih :attribute tidak valid.',
    'numeric'              => 'Bagian :attribute harus berupa angka.',
    'regex'                => 'Bagian format :attribute tidak valid.',
    'required'             => 'Bagian :attribute dibutuhkan.',
    'required_if'          => 'Bagian :attribute dibutuhkan saat :other adalah :value.',
    'required_with'        => 'Bagian :attribute dibutukan saat :values adalah sekarang.',
    'required_with_all'    => 'Bagian :attribute dibutukan saat :values adalah sekarang.',
    'required_without'     => 'Bagian :attribute dibutukan saat :values adalah bukan sekarang.',
    'required_without_all' => 'Bagian :attribute dibutukan saat tidak ada :values sekarang.',
    'same'                 => 'Bagian :attribute dan :other harus sama.',
    'size'                 => [
        'numeric' => 'Bagian :attribute harus sebesar :size.',
        'file'    => 'Bagian :attribute harus sebesar :size kilobytes.',
        'string'  => 'Bagian :attribute harus sebanyak :size karakter.',
        'array'   => 'Bagian :attribute harus terdapat :size.',
    ],
    'string'               => 'Bagian :attribute harus berupa string.',
    'timezone'             => 'Bagian :attribute harus merupakan zona valid.',
    'unique'               => 'Bagian :attribute sudah terisi, silahkan isi dengan yang lain.',
    'url'                  => 'Bagian format :attribute tidak valid.',
    'captcha'              => 'nilai reCAPTCHA tidak valid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
