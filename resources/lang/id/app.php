<?php

return [
    'home'                              => 'Beranda',
    'master_data'                       => 'Master Data',
    'my_profile'                        => 'Profil Saya',
    'active_sessions'                   => 'Sesi Aktif',
    'copyright_1'                       => 'Powered by',
    'copyright_2'                       => 'All Rights Reserved.',
    'users'                             => 'Pengguna',
    'users_sm'                          => 'pengguna',
    'roles_and_permissions'             => 'Roles & Permissions',
    'roles'                             => 'Roles',
    'permissions'                       => 'Permissions',
    'settings'                          => 'Pengaturan',
    'general'                           => 'Umum',
    'auth_and_registration'             => 'Otentifikasi & Registrasi',
    'notifications'                     => 'Notifikasi',
    'action'                            => 'Aksi',
    'action_sm'                         => 'aksi',
    "actions_sm"                        => 'aksi',
    "opd"                               => 'Instansi / OPD',
    "unit"                              => 'Unit Kerja',
    "golongan"                          => 'Golongan',
    "Eselon"                            => 'Eselon',
    "nomor"                             => 'Nomor',
    "opd_name"                          => 'Nama OPD',
    "unit_name"                         => 'Nama Unit Kerja',
    "nama"                              => 'Nama',
    "nip"                               => 'NIP',
    "golongan"                          => 'Golongan',
    "eselon"                            => 'Eselon',
    "pegawai"                           => 'Pegawai',
    "jenis_pangkat"                     => 'Jenis Pangkat',
    "page_not_found"                    => 'Halaman Tidak Ditemukan',
    "page_error_500"                    => 'Halaman Internal Error',
    "page_error_503"                    => 'Halaman Internal Error',
    "page_error_403"                    => 'Halaman Terbatas',

    // Activity Log Section
    'activity_log'                      => 'Catatan Aktifitas',
    'activity_log_sm'                   => 'catatan aktifitas',
    'search_for_action'                 => 'Pencarian...',
    'latest_activity'                   => 'Aktifitas terakhir',
    'no_activity_from_this_user_yet'    => 'Belum ada aktifitas tentang pengguna ini.',
    'view_all'                          => 'Lihat Semua',

    'user'                              => 'Pengguna',
    'user_sm'                           => 'pengguna',
    'ip_address'                        => 'Alamat IP',
    'message'                           => 'Pesan',
    'log_time'                          => 'Catatan Waktu',
    'more_info'                         => 'Info Lebih Lanjut',
    'view_activity_log'                 => 'Lihat Catatan Aktivitas',
    'user_agent'                        => 'Agen Pengguna',

    // Dashboard
    'welcome'                           => 'Selamat Datang',
    'dashboard'                         => 'Dashboard',
    'new_users_this_month'              => 'Pengguna bulan ini',
    'view_all_users'                    => 'Lihat semua pengguna',
    'total_users'                       => 'Jumlah pengguna',
    'view_details'                      => 'Lihat detail',
    'banned_users'                      => 'Pengguna terblokir',
    'unconfirmed_users'                 => 'Pengguna belum terkonfirmasi',
    'registration_history'              => 'Riwayat registrasi',
    'latest_registrations'              => 'Registrasi terakhr',
    'no_records_found'                  => 'Tidak ada data.',
    'update_profile'                    => 'Perbarui Profil',
    'my_sessions'                       => 'Sesi saya',
    'activity'                          => 'Aktifitas',
    'last_two_weeks'                    => 'Dua minggu terakhir',
    'previous'                          => 'Sebelumnya',
    'next'                              => 'Berikutnya',
    'first_page'                        => 'Halaman Pertama',
    'last_page'                         => 'Halaman Terakhir',

    // Auth
    'sign_up'                           => 'Daftar',
    'login'                             => 'Login Ke Sistem',
    'log_in'                            => 'Login',
    'log_me_in'                         => 'Masukan Saya Ke Sistem',
    'logout'                            => 'Logout',
    'email_or_username'                 => 'Email atau Username',
    'password'                          => 'Password',
    'i_forgot_my_password'              => 'Lupa password',
    'remember_me'                       => 'Ingat saya?',
    'dont_have_an_account'              => 'Tidak mempunyai akun?',
    'already_have_an_account'           => 'Sudah mempunyai akun?',
    'register'                          => 'Daftar Baru',
    'email'                             => 'Email',
    'your_email'                        => 'E-Mail anda',
    'username'                          => 'Username',
    'confirm_password'                  => 'Konfirmasi Password',
    'i_accept'                          => 'Saya Setuju',
    'terms_of_service'                  => 'Syarat dan Ketentuan Layanan',
    'close'                             => 'Tutup',
    'validate'                          => 'Validasi',
    'reset_password'                    => 'Reset Password',
    'back'                              => 'Kembali',
    'back_login'                        => 'Kembali Ke Halaman Login',
    'reset_your_password'               => 'Reset Password Anda',
    'new_password'                      => 'Password Baru',
    'confirm_new_password'              => 'Konfirmasi Password Baru',
    'update_password'                   => 'Perbarui Password',
    'forgot_your_password'              => 'Lupa Password Anda?',
    'please_provide_your_email_below'   => 'Silahkan masukkan email anda dibawah dan kami akan mengirimkan link reset password.',
    'pick_new_password_below'           => 'Silahkan masukkan email anda dan masukkan passwor baru dibawah.',

    // 2FA
    'two_factor_authentication'             => 'Otentifikasi Dua-Langkah',
    'two_factor_phone_verification'         => 'Otentifikasi Dua-Langkah Telpon',
    'phone_verification'                    => 'Telpon Verifikasi',
    'we_have_sent_you_a_verification_token' => 'Kami telah mengirimkan verifikasi token melalui SMS. Silahkan masukkan nomor telpon untuk memverifikasi nomor telpon.',
    'verify'                                => 'Memeriksa',
    'verifying'                             => 'Sedang Memverifikasi...',
    'resend_token'                          => 'Kirim Ulang Token',
    'sending'                               => 'Sedang mengirim...',
    'token'                                 => 'Token',
    'authy_2fa_token'                       => 'Authy 2FA Token',
    'enable_disable_2fa'                    => 'Aktifkan/Nonaktifkan Otentifikasi Dua-Langkah untuk aplikasi.',

    // Permissions
    'create_new_permission'                 => 'Buat Permission Baru',
    'permission_details'                    => 'Detail Permission',
    'edit'                                  => 'Ubah',
    'create'                                => 'Buat',
    'name'                                  => 'Nama',
    'permission_name'                       => 'Nama Permission',
    'display_name'                          => 'Nama Tampilan',
    'description'                           => 'Deskripsi',
    'update_permission'                     => 'Perbarui Permission',
    'create_permission'                     => 'Buat Permission Baru',
    'available_system_permissions'          => 'sistem permissions yang tersedia',
    'add_permission'                        => 'Tambah Permission',
    'edit_permission'                       => 'Ubah Permission',
    'delete_permission'                     => 'Hapus Permission',
    'please_confirm'                        => 'Konfirmasi',
    'are_you_sure_delete_permission'        => 'Apakah anda yakin akan menghapus permission ini?',
    'yes_delete_it'                         => 'Ya, hapus!',
    'save_permissions'                      => 'Simpan Permissions',

    // Roles
    'create_new_role'                       => 'Buat Role Baru',
    'role_details'                          => 'detail role',
    'role_details_big'                      => 'Role Detail',
    'role_name'                             => 'Nama Role',
    'update_role'                           => 'Perbarui Role',
    'create_role'                           => 'Buat Role',
    'available_system_roles'                => 'sistem role tersedia',
    'add_role'                              => 'Tambah Role',
    'users_with_this_role'                  => '# banyak pengguna dengan role ini',
    'edit_role'                             => 'Ubah Role',
    'delete_role'                           => 'Hapus Role',
    'are_you_sure_delete_role'              => 'Apakah anda yakin akan menghapus role ini?',

    // Settings
    'authentication_settings'               => 'Pengaturan Otentikasi',
    'authentication'                        => 'Otentikasi',
    'registration'                          => 'Registrasi',
    'general_settings'                      => 'Pengaturan Umum',
    'manage_general_system_settings'        => 'atur pengaturan sistem',
    'general_app_settings'                  => 'Pengaturan Umum Aplikasi',
    'app_name'                              => 'Nama Aplikasi',
    'update_settings'                       => 'Mengubah Pengaturan',
    'notification_settings'                 => 'Pengaturan Notifikasi',
    'manage_system_notification_settings'   => 'mengatur pengaturan notifikasi',
    'email_notifications'                   => 'Email Notifikasi',
    'notify_admin_when_user_signs_up'       => 'Kirim email ke Administrators saat pengguna meregistrasi.',
    'sign_up_notification'                  => 'Notifikasi registrasi',
    'yes'                                   => 'Ya',
    'no'                                    => 'Tidak',
    'allow_remember_me'                     => 'Ijinkan "Ingat Saya"',
    'should_remember_me_be_displayed'       => 'Haruskah pilihan \'Ingat Saya\' ditampilkan di login form?',
    'forgot_password'                       => 'Lupa Password',
    'enable_disable_forgot_password'        => 'Aktifkan/Nonaktifkan fitur lupa password.',
    'reset_token_lifetime'                  => 'Reset Token Lifetime',
    'number_of_minutes'                     => 'Jumlah banyaknya menit token reset password untuk valid.',
    'to_utilize_recaptcha_please_get'       => 'Untuk mengubah Google reCAPTCHA, silahkan dapatkan key Google',
    'site_key'                              => 'Site Key',
    'secret_key'                            => 'Secret Key',
    'from'                                  => 'dari',
    'recaptcha_website'                     => 'website reCAPTCHA',
    'and_update_your'                       => 'dan update',
    'and'                                   => 'dan',
    'environment_variables_inside'          => 'environment variables inside',
    'file'                                  => 'file',
    'disable'                               => 'Nonaktifkan',
    'enable'                                => 'Aktifkan',
    'allow_registration'                    => 'Bolehkan Registrasi',
    'terms_and_conditions'                  => 'Syarat dan Ketentuan',
    'the_user_has_to_confirm'               => 'Pengguna harus menyetujui Syarat dan Kondisi untuk membuat pengguna baru.',
    'email_confirmation'                    => 'Konfirmasi Email',
    'require_email_confirmation'            => 'Membutuhkan konfirmasi email untuk registrasi pengguna baru.',
    'authentication_throttling'             => 'Authentication Throttling',
    'throttle_authentication'               => 'Throttle Authentication',
    'should_the_system_throttle_authentication_requests' => 'Should the system throttle authentication attempts?',
    'maximum_number_of_attempts'            => 'Jumlah Maximum Percobaan',
    'max_number_of_incorrect_login_attempts' => 'Jumlah Maximum dari percobaan yang salah sebelum terkunci.',
    'lockout_time'                          => 'Waktu Terkunci',
    'num_of_minutes_to_lock_the_user'       => 'Jumlah dalam menit untuk pengguna terkunci dari banyak kesalahan percobaan password.',
    'in_order_to_enable_2fa'                => 'Untuk mengaktifkan Otentifikasi Dua-Langkah, anda harus registrasi',
    'new_application_on'                    => 'aplikasi baru ',
    'authy_website'                         => 'Authy Website',
    'environment_variable_inside'           => 'environment variable inside',
    'disabling'                             => 'Menonaktifkan...',
    'enabling'                              => 'Mengaktifkan...',

    // Users
    'add_user'                              => 'Tambah Pengguna',
    'create_new_user'                       => 'Buat Pengguna Baru',
    'user_details'                          => 'Detail Pengguna',
    'user_details_other'                    => 'Informasi profil pengguna',
    'create_user'                           => 'Buat Pengguna Baru',
    'edit_user'                             => 'Ubah User',
    'edit_user_details'                     => 'ubah detail pengguna',
    'details'                               => 'Detail',
    'search_for_users'                      => 'Mencari pengguna...',
    'full_name'                             => 'Nama Lengkap',
    'registration_date'                     => 'Tanggal Registrasi',
    'status'                                => 'Status',
    'n_a'                                   => 'Masih Kosong',
    'user_sessions'                         => 'Sesi Pengguna',
    'view_user'                             => 'Lihat Pengguna',
    'delete_user'                           => 'Hapus Pengguna',
    'are_you_sure_delete_user'              => 'Apakah anda yakin menghapus user ini?',
    'yes_delete_him'                        => 'Ya, hapus!',
    'edit_profile_details'                  => 'ubah detail profil',
    'active_sessions_sm'                    => 'sesi aktif',
    'sessions'                              => 'Sesi',
    'last_activity'                         => 'Aktifitas Terakhir',
    'invalidate_session'                    => 'Sesi Tidak Valid',
    'are_you_sure_invalidate_session'       => 'Apakah anda yakin membatalkan sesi ini?',
    'yes_proceed'                           => 'Ya, lanjutkan!',
    'contact_informations'                  => 'Informasi Kontak',
    'additional_informations'               => 'Tambahan Informasi',
    'birth'                                 => 'Tanggal Lahir',
    'address'                               => 'Alamat',
    'last_logged_in'                        => 'Terakhir Login',
    'complete_activity_log'                 => 'Catatan Aktivitas Lengkap',
    'date'                                  => 'Tanggal',
    'login_details'                         => 'Detail Login Lainnya',
    'login_details_other'                   => 'Digunakan untuk mengautentikasi aplikasi.',
    'optional'                              => 'optional',
    'new'                                   => 'Baru',
    'new_sm'                                => 'baru',
    'leave_blank_if_you_dont_want_to_change' => 'Biarkan kosong jika tidak ada perubahan',
    'update_details'                        => 'Ubah Detail',
    'avatar'                                => 'Avatar',
    'change_photo'                          => 'Ubah Foto',
    'upload_photo'                          => 'Upload Foto',
    'gravatar'                              => 'Gravatar',
    'cancel'                                => 'Batal',
    'save'                                  => 'Simpan',
    'no_photo'                              => 'Tidak ada Foto',
    'role'                                  => 'Role',
    'first_name'                            => 'Nama Depan',
    'last_name'                             => 'Nama Belakang',
    'date_of_birth'                         => 'Tanggal Lahir',
    'phone'                                 => 'Ponsel',
    'country'                               => 'Negara',
    'in_order_to_enable_2fa_you_must'       => 'In order to enable Two-Factor Authentication, you must install',
    'application_on_your_phone'             => 'application on your phone',
    'country_code'                          => 'Kode Negara',
    'cell_phone'                            => 'Ponsel',
    'phone_without_country_code'            => 'Telpon tanpa kode negara',
    'please_confirm_your_email_first'       => 'Silahkan konfirmasi email anda.',
    'your_account_is_banned'                => 'Akun anda di blokir oleh administrator.',
    '2fa_token_invalid'                     => 'Token 2FA tidak valid!',
    'account_create_confirm_email'          => 'Akun anda telah berhasil dibuat! Silahkan konfirmasi email anda untuk masuk ke sistem.',
    'account_created_login'                 => 'Akun anda telah berhasil dibuat! Silahkan masuk ke sistem.',
    'email_confirmed_can_login'             => 'Email terkonfirmasi. Anda dapat Login ke sistem sekarang.',
    'wrong_confirmation_token'              => 'Token konfirmasi salah.',
    'password_reset_email_sent'             => 'Reset password telah dikirim ke email. Silahkan cek email inbox atau spam anda.',
    'only_users_with_account_can_login'     => 'Hanya pengguna yang terdaftar yang dapat masuk.',
    'you_have_to_provide_email'             => 'Anda harus memasukan alamat email.',
    'permission_created_successfully'       => 'Permission berhasil dibuat.',
    'permission_updated_successfully'       => 'Permission berhasil diubah.',
    'permission_deleted_successfully'       => 'Permission berhasil dihapus.',
    'permissions_saved_successfully'        => 'Permissions disimpan.',
    'profile_updated_successfully'          => 'Profile diubah.',
    'avatar_changed'                        => 'Avatar diubah.',
    'login_updated'                         => 'Detail Login berhasil diubah.',
    '2fa_already_enabled'                   => 'Otentifikasi Dua-Langkah berhasil diaktifkan.',
    '2fa_enabled'                           => 'Otentifikasi Dua-Langkah berhasil diaktifkan.',
    '2fa_not_enabled_for_this_user'         => 'Otentifikasi Dua-Langkah tidak diaktifkan untuk pengguna ini.',
    '2fa_disabled'                          => 'Otentifikasi Dua-Langkah berhasil dinonaktifkan.',
    'session_invalidated'                   => 'Session invalidated successfully.',
    'role_created'                          => 'Role berhasil dibuat.',
    'role_updated'                          => 'Role berhasil diubah.',
    'role_deleted'                          => 'Role berhasil dihapus.',
    'settings_updated'                      => 'Settings berhasil diubah.',
    'recaptcha_enabled'                     => 'reCAPTCHA berhasil diaktifkan.',
    'recaptcha_disabled'                    => 'reCAPTCHA berhasil dinonaktifkan.',
    'enable_disable_captcha_during_registration' => 'Aktifkan/Nonaktifkan Google reCAPTCHA selama registrasi.',
    'all'                                   => 'Semua',
    'Active'                                => 'Aktif',
    'Banned'                                => 'Terblokir',
    'Unconfirmed'                           => 'Belum terkonfirmasi',
    'user_created'                          => 'Pengguna sukses dibuat.',
    'user_updated'                          => 'Pengguna sukses diubah.',
    'you_cannot_delete_yourself'            => 'Anda tidak dapat menghapus diri sendiri.',
    'user_deleted'                          => 'Pengguna sukses dihapus.',
    '2fa_already_enabled_user'              => 'Otentifikasi Dua-Langkah telah diaktifkan untuk pengguna ini.',
    '2fa_not_enabled_user'                  => 'Otentifikasi Dua-Langkah tidak diakftifkan untuk pengguna ini.',
    'you_have_to_accept_tos'                => 'Anda harus menyetujui Ketentuan Layanan.',
    'permission_already_exists'             => 'Permission dengan nama ini telah ada!',

    //Emails
    'hi'                                    => 'Hi',
    'registration_confirmation'             => 'Konfirmasi Registrasi',
    'new_user_was_registered_on'            => 'Pengguna baru baru meregistrasi :app.',
    'website'                               => 'website',
    'to_view_details_visit_link_below'      => 'Untuk melihat detail pengguna klik link berikut:',
    'many_thanks'                           => 'Terima kasih banyak',
    'request_for_password_reset_made'       => 'Email ini dikirim kepada anda karena anda telah meminta untuk mereset password akun anda di Sistem Lapor Jaksa.',
    'if_you_did_not_requested'              => 'Jika anda tidak merasa mengirimkan permintaan reset password, dimohon jangan lakukan apa-apa.',
    'click_on_link_below'                   => 'Silahkan klik link berikut untuk mereset password anda:',
    'if_you_cant_click'                     => 'Jika anda mengalami kendala dalam mengklik tombol ":button", anda bisa mengklik alamat URL berikut dibawah atau dengan melakukan Copy dan Paste alamat URL berikut di browser anda :',
    'thank_you_for_registering'             => 'Terima kasih telah mendaftarkan akun :app.',
    'confirm_email_on_link_below'           => 'Silahkan konfirmasi kembali email anda dengan mengklik tombol Konfirmasi Email',
    'confirm_email'                         => 'Konfirmasi Email',
    'new_user_registration'                 => 'Registrasi Pengguna Baru',
    'thank_you_for_using_our_app'           => 'Terima kasih telah menggunakan aplikasi kami.',
    'all_rights_reserved'                   => 'All rights reserved.',
    'regards'                               => 'Salam dari kami',
    'months' => [
        1 => 'Januari',
        2 => 'Febuari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    ],
    'avatar_not_changed'                    => 'Gambar Avatar tidak dapat diubah. Silahkan coba kembali.',
    'unknown'                               => 'Tidak diketahui',
    'device'                                => 'Alat',
    'browser'                               => 'Browser',
    'invalid_social_auth'                   => "Terdapat masalah dengan otentifikasi. Silahkan coba kembali.",
    'impersonate'                           => 'Meniru',
    'impersonate_user'                      => 'Meniru Pengguna',
    'stop_impersonating'                    => 'Berhenti meniru',

    'categories'         => 'Kategori',
    'category_name'      => 'Nama Kategori',
    'category_slug'      => 'Kategori Slug',
    'add_categories'     => 'Tambah Kategori',
    'create_categories'  => 'Buat Kategori',
    'update_categories'  => 'Ubah Kategori',
    'categories_details' => 'Detail Kategori',
    'categories_created' => 'Kategori Berhasil Dibuat',
    'categories_updated' => 'Kategori Berhasil Diubah',
    'categories_deleted_successfully' => 'Kategori Berhasil Dihapus',
    'create_aparat'        => 'Buat Baru Aparatur Desa',
    'update_aparat'        => 'Ubah Aparatur Desa',
    'create_pendidikan'    => 'Buat Jenis Pendidikan',
    'update_pendidikan'    => 'Ubah Jenis Pendidikan',
    'create_desa'          => 'Buat Desa',
    'update_desa'          => 'Ubah Desa',
    'create_arsip_antar'   => 'Buat Arsip Antar Desa',
    'update_arsip_antar'   => 'Ubah Arsip Antar Desa',
    'create_arsip_kemitraan' => 'Buat Arsip Kemitraan',
    'update_arsip_kemitraan' => 'Ubah Arsip Kemitraan',
    'create_arsip_lainnya'   => 'Buat Arsip Lainnya',
    'update_arsip_lainnya'   => 'Ubah Arsip Lainnya',
    'create_arsip_lebih'     => 'Buat Arsip Lebih Dari Dua Desa',
    'update_arsip_lebih'     => 'Ubah Arsip Lebih Dari Dua Desa',
    'update'               => 'Ubah',
    'change_status'        => 'Ubah Status',
    'reset'                => 'Reset',
    'search'               => 'Cari'
];