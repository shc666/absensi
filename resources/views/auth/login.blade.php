<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<!-- begin::Head -->
	<head>
		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="">
		<!--end::Base Path -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8" />
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@lang('app.login') | {{ config('app.name', 'Laravel') }}</title>

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<!--end::Fonts -->
		<!--begin::Page Custom Styles(used by this page) -->
		<link href="{{ url('assets/css/demo1/pages/login/login-4.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Custom Styles -->
		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{ url('assets/vendors/global/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles -->
		<!--begin::Layout Skins(used by all pages) -->
		<link href="{{ url('assets/css/demo1/skins/header/base/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/skins/brand/dark.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/skins/aside/dark.css') }}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
		<!-- Custom Stylesheet -->
		<link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="{{ url('assets/media/logos/favicon.ico') }}" />
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
		<!-- begin:: Page -->
		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{ url('assets/media/bg/bg-3.jpg') }});">
					<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
						<div class="kt-login__container">
							<div class="kt-login__logo">
								<a href="#">
									<img src="{{ url('assets/media/logos/logo-mini-md.png') }}">
								</a>
							</div>
							<div class="kt-login__signin">
								<div class="kt-login__head">
									<h3 class="kt-login__title">@lang('app.login')</h3>
								</div>

								@include('partials.messages')

								<form class="kt-form" action="{{ route('post.login') }}" method="POST" id="login-form">
									@csrf
									<div class="input-group">
										<input class="form-control login-v4"
												type="text" 
												placeholder="@lang('app.email_or_username')" 
												name="username" 
												autocomplete="off"
												id="username">
									</div>
									<div class="input-group">
										<input class="form-control login-v4" 
												type="password" 
												placeholder="@lang('app.password')" 
												name="password">
									</div>
									<div class="row kt-login__extra">
										<div class="col">
											<label class="kt-checkbox">
												<input type="checkbox" name="remember"> Ingat saya?
												<span></span>
											</label>
										</div>
									</div>
									<div class="kt-login__actions">
										<button style="width:100%" type="submit" id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-wide kt-login__btn-primary">@lang('app.log_in')</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->
		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{ url('assets/vendors/global/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/scripts.bundle.js') }}" type="text/javascript"></script>
		<!--end::Global Theme Bundle -->
		<!--begin::Page Scripts(used by this page) -->
		<script src="{{ url('assets/js/as/app.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/pages/login/login-general.js')}}" type="text/javascript"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		{!! Toastr::message() !!}
		<!--end::Page Scripts -->
	</body>
	<!-- end::Body -->
</html>