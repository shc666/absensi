<!-- begin:: Footer -->
<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid" style="justify-content:center !important;">
        <div class="kt-footer__copyright">
            © {{ date('Y') }} Chandrayana Putra
        </div>
    </div>
</div>
<!-- end:: Footer -->