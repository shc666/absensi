<html>
    <head>
        <style>
            .body{font-size: 12pt;font-family: "Times New Roman", Times, serif;border-spacing: 0;}
            div{text-align: center;padding-bottom: 2px;}
            h2{font-size: 13pt;text-align: center;margin: 0px 5px 2px -80px !important;}
            h3{font-size: 13pt;text-align: center;margin: 15px 5px 2px -80px !important;font-weight: normal;}
            h4{font-size: 12pt;text-align: center;margin: 0px 5px 2px -80px !important;font-weight: normal;}
            h5{font-size: 10pt;text-align: center;margin: 0px 5px 2px -80px !important;font-weight: normal;}
            hr{display: block;height: 2px;border: 0;border-top: 7px double #000;margin: 0px 0px 0px 0px;padding: 0;}
            hr.top{margin-top: 5px;margin-bottom: 0px;border-top: 1px solid #000;}
            #table{border-collapse:collapse !important;border-bottom: 1px solid #000;page-break-before: auto;}
            .header{font-size: 12pt;margin-top: 20px;text-align: center;}
            .text{text-indent: 0.3in;text-align: justify;margin-bottom: 0px!important;}
            /* Custom Table Laporan Individu */
            .tgi  {border-collapse:collapse;border-spacing:0;border-color:#93a1a1;}
            .tgi td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#002b36;background-color:#fdf6e3;}
            .tgi th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#fdf6e3;background-color:#657b83;}
            .tgi .tg-deio{background-color:#657b83;color:#fdf6e3;border-color:#000000;text-align:center;vertical-align:middle}
            .tgi .tg-xwyw{border-color:#000000;text-align:center;vertical-align:middle}
            .tgi .tg-vhtn{background-color:#ffffff;border-color:#000000;text-align:center;vertical-align:middle}
        </style>
    </head>
    <body>
        <page backtop="15mm" backbottom="25mm" backleft="20mm" backright="15mm" class="body">
            <h2>Laporan Absensi Individu Bulan {{ $indonesian_format }}</h2>
            <br>
            <h3> NIP / Nama Pegawai : {{ $employees['pegawai'] }}</h3>
            <br>
            <h2>Dinas {{ $employees['opd'] }}, Unit Kerja {{ $employees['unit'] }}</h2>
            <br>
            {{-- Begin Custom Table --}}
            <table class="tgi" style="undefined;table-layout: fixed; width: 894px" id="table">
                <colgroup>
                    <col style="width: 52px">
                    <col style="width: 122px">
                    <col style="width: 142px">
                    <col style="width: 72px">
                    <col style="width: 72px">
                    <col style="width: 141px">
                    <col style="width: 81px">
                    <col style="width: 131px">
                    <col style="width: 121px">
                </colgroup>
                <thead>
                    <tr>
                        <th class="tg-xwyw" rowspan="2">No</th>
                        <th class="tg-xwyw" rowspan="2">Hari</th>
                        <th class="tg-xwyw" rowspan="2">Tanggal</th>
                        <th class="tg-xwyw" colspan="2">Absen</th>
                        <th class="tg-xwyw" rowspan="2">Jam Kerja Pegawai</th>
                        <th class="tg-xwyw" rowspan="2">Terlambat</th>
                        <th class="tg-xwyw" rowspan="2">Pulang Mendahului</th>
                        <th class="tg-xwyw" rowspan="2">Keterangan</th>
                    </tr>
                    <tr>
                        <td class="tg-deio">Masuk</td>
                        <td class="tg-deio">Pulang</td>
                    </tr>
                </thead>
                <tbody>
                    @for($day=1; $day <= $day_value; $day++)
                    @if($day>=1 AND $day<=9)
                        @php($selected_day = $date."-0".$day)
                    @else
                        @php($selected_day = $date."-".$day)
                    @endif
                    <tr>
                        <td class="tg-vhtn">{{ $day }}</td>
                        <td class="tg-vhtn">
                            {{ App\Helpers\Format::getDayByFormat($selected_day, "Y-m-d") }}
                        </td>
                        <td class="tg-vhtn">{{ $day }} {{ App\Helpers\Format::getMonthLong($selected_month) }} {{ $selected_year }}</td>
                        <td class="tg-vhtn">
                            @foreach($attendances as $attendance)
                                @php($day_value = date("d", strtotime($attendance['tanggal'])))
                                @if($day_value == $day)
                                    @if($attendance['absen_in'] != '')
                                        {{ $attendance['absen_in'] }}
                                    @else
                                        -
                                    @endif
                                @endif
                            @endforeach
                        </td>
                        <td class="tg-vhtn">
                            @foreach($attendances as $attendance)
                                @php($day_value = date("d", strtotime($attendance['tanggal'])))
                                @if($day_value == $day)
                                    @if($attendance['absen_out'] != '')
                                        {{ $attendance['absen_out'] }}
                                    @else
                                        -
                                    @endif
                                @endif
                            @endforeach
                        </td>
                        <td class="tg-vhtn"> - </td>
                        <td class="tg-vhtn"> - </td>
                        <td class="tg-vhtn"> - </td>
                        <td class="tg-vhtn"> - </td>
                    </tr>
                    @endfor
                </tbody>
            </table>
            {{-- End Custom Table --}}
            <br>
        </page>
    </body>
</html>