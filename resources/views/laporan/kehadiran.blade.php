<div class="kt-portlet .kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin::Section-->
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="table-responsive" style="overflow-x:auto;">
                    {{-- Begin Custom Table --}}
                    <table class="tg" style="undefined;table-layout: fixed; width: 489px">
                        <colgroup>
                            <col style="width: 56px">
                            <col style="width: 241px">
                            @for($day=1; $day <= $day_value; $day++)
                                <col style="width: 61px">
                            @endfor
                            <col style="width: 131px">
                        </colgroup>
                        <thead>
                            <tr>
                                <th class="tg-qpjl">No</th>
                                <th class="tg-qpjl">NIP / Nama</th>
                                @for($day=1; $day <= $day_value; $day++)
                                    <th class="tg-l49g">{{ $day }}</th>
                                @endfor
                                <th class="tg-xwyw">Total Kehadiran</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach($employees as $employee)
                                <tr>
                                    <td class="tg-8hay">{{ $no++ }}</td>
                                    <td class="tg-8hay">{{ $employee['pegawai'] }}</td>
                                    @for($day=1; $day <= $day_value; $day++)
                                        <td class="tg-bvxb" title="">
                                            @if($day>=1 AND $day<=9)
                                                @php($selected_day = $date."-0".$day)
                                            @else
                                                @php($selected_day = $date."-".$day)
                                            @endif
                                            @php($day_name = date("D", strtotime($selected_day)))

                                            @foreach($weekly_holidays as $weekly_holiday)
                                                @if($day_name == $weekly_holiday['day'])
                                                    <button class="btn btn-bold btn-sm btn-font-sm btn-label-danger" 
                                                    data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Hari Libur (Sabtu dan Minggu)">L</button>
                                                @endif
                                            @endforeach

                                            @foreach($attendances as $attendance)
                                                @if($attendance['id_pegawai'] == $employee['id_pegawai'])
                                                    @if($attendance['tanggal'] == $selected_day)
                                                        @if($attendance['absen_in'] != '' && $attendance['absen_out'] != '')
                                                            <button class="btn btn-bold btn-sm btn-font-sm btn-label-success" 
                                                            data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="{{ $attendance['absen_in']." - ".$attendance['absen_out'] }}">H</button>
                                                        @else
                                                            <button class="btn btn-bold btn-sm btn-font-sm btn-label-warning"
                                                            data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Absen Tidak Lengkap"> - </button>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endforeach
                                        </td>
                                    @endfor
                                    <td class="tg-jbyd">
                                        @foreach($counts as $value)
                                            @if($value['id_pegawai'] == $employee['id_pegawai'])
                                                {{ $value['total_absen'] }}
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- End Custom Table --}}
                </div>
            </div>
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="kt-section">
            <div class="kt-section__content">
                <form target="_blank" action="{{ route('cetak.tipe-laporan', $tipe) }}" method="POST" accept-charset="UTF-8">
                    @csrf
                    <input type="hidden" name="tipe" value="{{ $tipe }}">
                    <input type="hidden" name="selected_month" value="{{ $selected_month }}">
                    <input type="hidden" name="selected_year" value="{{ $selected_year }}">
                    <input type="hidden" name="selected_opd" value="{{ $selected_opd }}">
                    <input type="hidden" name="selected_unit" value="{{ $selected_unit }}">
                    {{ method_field('PUT') }}
                    <button type="submit" class="btn btn-outline-info btn-elevate btn-pill">
                        <i class="la la-print"></i> Cetak Laporan
                    </button>
                </form>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>