@extends('layouts.app')

@section('page-title')
    Laporan Absensi {{ $header }}
@endsection

@section('content')
    <div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor arsip-wrapper" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Laporan Absensi</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">{{ $header }}</span>
                </div>
            </div>
        </div>
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-laporan">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Begin Portlet -->
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="kt-font-brand flaticon-doc"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    Laporan {{ $header }}
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <form id="kt_form_1" method="POST">
                                @csrf
                                <input type="hidden" name="tipe" value="{{ $tipe }}">
                                <div class="kt-section__body kt-laporan__filters">
                                    <div class="kt-laporan__filter kt-laporan__filter-1">
                                        <div class="form-group">
                                            <label class="col-form-label">Bulan</label>
                                            <div class="">
                                                <select class="form-control kt-select2" name="select_month" id="kt_select2_1-13">
                                                    <option value=""></option>
                                                @foreach($months as $month)
                                                    <option value="{{ $month['value'] }}">{{ $month['text'] }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Tahun</label>
                                            <div class="">
                                                <select class="form-control kt-select2" name="select_year" id="kt_select2_1-14">
                                                    <option value=""></option>
                                                @foreach($years as $year)
                                                    <option value="{{ $year->tahun }}">{{ $year->tahun }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-laporan__filter kt-laporan__filter-1">
                                        <div class="form-group">
                                            <label class="col-form-label">Instansi (OPD)</label>
                                            <div class="">
                                                <select class="form-control kt-select2" name="select_opd" id="kt_select2_1-6">
                                                    <option value=""></option>
                                                @foreach($opds as $opd)
                                                    <option value="{{ $opd->id }}">{{ $opd->nama_opd }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @if($tipe == 1 || $tipe == 3 || $tipe == 4)
                                            <div class="form-group">
                                                <label class="col-form-label">Unit Kerja</label>
                                                <div class="">
                                                    <select class="form-control kt-select2" name="select_unit" id="kt_select2_1-7">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                            </div>
                                        @else
                                            <div class="form-group">
                                                <label class="col-form-label">Pegawai</label>
                                                <div class="">
                                                    <select class="form-control kt-select2" name="select_employee" id="kt_select2_1-8">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="kt-form kt-portlet__foot">
                                    <div class="kt-form__actions kt-form__actions--right">
                                        <button type="button" class="btn btn-danger mr-1" id="reset" onclick="reset_button();">
                                            <i class="flaticon2-reload"></i>
                                            {{ trans('app.reset') }}
                                        </button>
                                        <button type="submit" class="btn btn-success" id="submit">
                                            <i class="flaticon2-search"></i>
                                            {{ trans('app.search') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <hr/>
                    </div>
                    <!-- End Portlet -->
                    <div id="table_laporan">
                        {{-- Ajax Table Laporan --}}
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            @if($tipe == 1 || $tipe == 3 || $tipe == 4)
            $("#kt_select2_1-6").change(function() {
                var opd_id = $("#kt_select2_1-6").val();
                var url = "{{ route('get.ajax-unit', ':opd_id') }}";
                urlajax = url.replace(":opd_id", opd_id);
                $.ajax({
                    type: "GET",
                    url: urlajax,
                    success: function(result){
                        $("#kt_select2_1-7").html(result);
                    }
                });
            });
            @else
            $("#kt_select2_1-6").change(function() {
                var opd_id = $("#kt_select2_1-6").val();
                var url = "{{ route('get.ajax-pegawai', ':opd_id') }}";
                urlajax = url.replace(":opd_id", opd_id);
                $.ajax({
                    type: "GET",
                    url: urlajax,
                    success: function(result){
                        $("#kt_select2_1-8").html(result);
                    }
                });
            });
            @endif

            $(document).on('submit', '#kt_form_1', function(event) {
                event.preventDefault();
                var form = $(this).serialize();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                $.ajax({
                    type: "POST",
                    url: "{{ route('post.tipe-laporan', $tipe) }}",
                    data: form,
                    dataType: 'html',
                })
                .done(function(html){
                    $("#table_laporan").html(html);
                })
                .fail(function(xhr){
                    console.log(xhr);
                })
            });
        });

        function reset_button()
        {
            $(".kt-select2").val(null).trigger('change');
        }
    </script>
@endsection