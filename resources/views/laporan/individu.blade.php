<div class="kt-portlet .kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin::Section-->
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="table-responsive" style="overflow-x:auto;">
                    {{-- Begin Custom Table --}}
                    <table class="tgi" style="undefined;table-layout: fixed; width: 894px">
                        <colgroup>
                            <col style="width: 52px">
                            <col style="width: 122px">
                            <col style="width: 142px">
                            <col style="width: 72px">
                            <col style="width: 72px">
                            <col style="width: 141px">
                            <col style="width: 81px">
                            <col style="width: 131px">
                            <col style="width: 121px">
                        </colgroup>
                        <thead>
                            <tr>
                                <th class="tg-xwyw" rowspan="2">No</th>
                                <th class="tg-xwyw" rowspan="2">Hari</th>
                                <th class="tg-xwyw" rowspan="2">Tanggal</th>
                                <th class="tg-xwyw" colspan="2">Absen</th>
                                <th class="tg-xwyw" rowspan="2">Jam Kerja Pegawai</th>
                                <th class="tg-xwyw" rowspan="2">Terlambat</th>
                                <th class="tg-xwyw" rowspan="2">Pulang Mendahului</th>
                                <th class="tg-xwyw" rowspan="2">Keterangan</th>
                            </tr>
                            <tr>
                                <td class="tg-deio">Masuk</td>
                                <td class="tg-deio">Pulang</td>
                            </tr>
                        </thead>
                        <tbody>
                            @for($day=1; $day <= $day_value; $day++)
                            @if($day>=1 AND $day<=9)
                                @php($selected_day = $date."-0".$day)
                            @else
                                @php($selected_day = $date."-".$day)
                            @endif
                            <tr>
                                <td class="tg-vhtn">{{ $day }}</td>
                                <td class="tg-vhtn">
                                    {{ App\Helpers\Format::getDayByFormat($selected_day, "Y-m-d") }}
                                </td>
                                <td class="tg-vhtn">{{ $day }} {{ App\Helpers\Format::getMonthLong($selected_month) }} {{ $selected_year }}</td>
                                <td class="tg-vhtn">
                                    @foreach($attendances as $attendance)
                                        @php($day_value = date("d", strtotime($attendance['tanggal'])))
                                        @if($day_value == $day)
                                            @if($attendance['absen_in'] != '')
                                                {{ $attendance['absen_in'] }}
                                            @else
                                                -
                                            @endif
                                        @endif
                                    @endforeach
                                </td>
                                <td class="tg-vhtn">
                                    @foreach($attendances as $attendance)
                                        @php($day_value = date("d", strtotime($attendance['tanggal'])))
                                        @if($day_value == $day)
                                            @if($attendance['absen_out'] != '')
                                                {{ $attendance['absen_out'] }}
                                            @else
                                                -
                                            @endif
                                        @endif
                                    @endforeach
                                </td>
                                <td class="tg-vhtn"> - </td>
                                <td class="tg-vhtn"> - </td>
                                <td class="tg-vhtn"> - </td>
                                <td class="tg-vhtn"> - </td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                    {{-- End Custom Table --}}
                </div>
            </div>
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="kt-section">
            <div class="kt-section__content">
                <form target="_blank" action="{{ route('cetak.tipe-laporan', $tipe) }}" method="POST" accept-charset="UTF-8">
                    @csrf
                    <input type="hidden" name="tipe" value="{{ $tipe }}">
                    <input type="hidden" name="selected_month" value="{{ $selected_month }}">
                    <input type="hidden" name="selected_year" value="{{ $selected_year }}">
                    <input type="hidden" name="selected_opd" value="{{ $selected_opd }}">
                    <input type="hidden" name="selected_employee" value="{{ $selected_employee }}">
                    {{ method_field('PUT') }}
                    <button type="submit" class="btn btn-outline-info btn-elevate btn-pill">
                        <i class="la la-print"></i> Cetak Laporan
                    </button>
                </form>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>