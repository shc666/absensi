<div class="kt-portlet .kt-portlet--mobile">
    <div class="kt-portlet__body" id="ajax">
        <!--begin::Section-->
        <div class="kt-section">
            <div class="kt-section__content">
                <div class="table-responsive">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                        <thead>
                            <tr>
                                <th>No</th>                                
                                <th>Kabupaten</th>
                                <th>Kecamatan</th>
                                <th>Desa</th>
                                <th>Jenis Kerjasama</th>
                                <th>Kategori</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            Rekap
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>