<html>
    <head>
        <style>
            .body{font-size: 12pt;font-family: "Times New Roman", Times, serif;border-spacing: 0;}
            div{text-align: center;padding-bottom: 2px;}
            h2{font-size: 13pt;text-align: center;margin: 0px 5px 2px -80px !important;}
            h3{font-size: 13pt;text-align: center;margin: 15px 5px 2px -80px !important;font-weight: normal;}
            h4{font-size: 12pt;text-align: center;margin: 0px 5px 2px -80px !important;font-weight: normal;}
            h5{font-size: 10pt;text-align: center;margin: 0px 5px 2px -80px !important;font-weight: normal;}
            hr{display: block;height: 2px;border: 0;border-top: 7px double #000;margin: 0px 0px 0px 0px;padding: 0;}
            hr.top{margin-top: 5px;margin-bottom: 0px;border-top: 1px solid #000;}
            #table{border-collapse:collapse !important;border-bottom: 1px solid #000;page-break-before: auto;}
            .header{font-size: 12pt;margin-top: 20px;text-align: center;}
            .text{text-indent: 0.3in;text-align: justify;margin-bottom: 0px!important;}
            /* Custom Table Laporan */
            .tg {border-collapse:collapse;border-spacing:0;border-color:#93a1a1;}
            .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#002b36;background-color:#fdf6e3;}
            .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#93a1a1;color:#fdf6e3;background-color:#657b83;}
            .tg .tg-8hay{font-family:"Lucida Console", Monaco, monospace !important;;background-color:#ffffff;color:#000000;border-color:#000000;text-align:center;vertical-align:top}
            .tg .tg-bvxb{background-color:#ffffff;color:#000000;border-color:#000000;text-align:center;vertical-align:top}
            .tg .tg-qpjl{font-family:"Lucida Console", Monaco, monospace !important;;color:#ffffff;border-color:#000000;text-align:center;vertical-align:middle}
            .tg .tg-l49g{color:#ffffff;border-color:#000000;text-align:center;vertical-align:top}
            .tg .tg-xwyw{border-color:#000000;text-align:center;vertical-align:middle}
            .tg .tg-jbyd{background-color:#ffffff;border-color:#000000;text-align:center;vertical-align:top}
            .libur{background-color: red;}
            .working-days{background-color: green;}
            .not-completed{background-color: yellow;}
        </style>
    </head>
    <body>
        <page backtop="15mm" backbottom="25mm" backleft="20mm" backright="15mm" class="body">
            <h2>Laporan Absensi Kehadiran Bulan {{ $indonesian_format }}</h2>
            <br>
            @isset($unit_name)
                <h2>Dinas {{ $opd_name }}, Unit Kerja {{ $unit_name }}</h2>
            @else
                <h2>Dinas {{ $opd_name }}</h2>
            @endisset
            <br>
            {{-- Begin Custom Table --}}
            <table class="tg" style="undefined;table-layout: fixed; width: 489px" id="table">
                <colgroup>
                    <col style="width: 56px">
                    <col style="width: 241px">
                    @for($day=1; $day <= $day_value; $day++)
                        <col style="width: 61px">
                    @endfor
                    <col style="width: 131px">
                </colgroup>
                <thead>
                    <tr>
                        <th class="tg-qpjl">No</th>
                        <th class="tg-qpjl">NIP / Nama</th>
                        @for($day=1; $day <= $day_value; $day++)
                            <th class="tg-l49g">{{ $day }}</th>
                        @endfor
                        <th class="tg-xwyw">Total Kehadiran</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($employees as $employee)
                        <tr>
                            <td class="tg-8hay">{{ $no++ }}</td>
                            <td class="tg-8hay">{{ $employee['pegawai'] }}</td>
                            @for($day=1; $day <= $day_value; $day++)
                                <td class="tg-bvxb" title="">
                                    @if($day>=1 AND $day<=9)
                                        @php($selected_day = $date."-0".$day)
                                    @else
                                        @php($selected_day = $date."-".$day)
                                    @endif
                                    @php($day_name = date("D", strtotime($selected_day)))

                                    @foreach($weekly_holidays as $weekly_holiday)
                                        @if($day_name == $weekly_holiday['day'])
                                            <span class="libur" title="Hari Libur (Sabtu dan Minggu)">L</button>
                                        @endif
                                    @endforeach
                                    @foreach($attendances as $attendance)
                                        @if($attendance['id_pegawai'] == $employee['id_pegawai'])
                                            @if($attendance['tanggal'] == $selected_day)
                                                @if($attendance['absen_in'] != '' && $attendance['absen_out'] != '')
                                                    <span class="working-days" title="{{ $attendance['absen_in']." - ".$attendance['absen_out'] }}">H</span>
                                                @else
                                                    <span class="not-completed" title="Absen Tidak Lengkap"> - </span>
                                                @endif
                                            @endif
                                        @endif
                                    @endforeach
                                </td>
                            @endfor
                            <td class="tg-jbyd">
                                @foreach($counts as $value)
                                    @if($value['id_pegawai'] == $employee['id_pegawai'])
                                        {{ $value['total_absen'] }}
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- End Custom Table --}}
            <br>
            <table>
                <thead>
                    <tr>
                        <td width="50"><strong>Keterangan: </strong></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="10">-</td>
                        <td>Absen Tidak Lengkap</td>
                    </tr>
                    <tr>
                        <td width="10">H</td>
                        <td>Hadir</td>
                    </tr>
                    <tr>
                        <td width="10">L</td>
                        <td>Hari Libur (Sabtu dan Minggu)</td>
                    </tr>
                </tbody>
            </table>
        </page>
    </body>
</html>