<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<!-- begin::Head -->
	<head>
		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="">
        <!--end::Base Path -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8" />
		<meta name="description" content="Updates and statistics">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-title') | {{ config('app.name', 'Sistem Kearsipan') }}</title>

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="{{ url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles -->
		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{ url('assets/vendors/global/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles -->
		<!--begin::Layout Skins(used by all pages) -->
		<link href="{{ url('assets/css/demo1/skins/header/base/dark.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/skins/brand/dark.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/css/demo1/skins/aside/dark.css') }}" rel="stylesheet" type="text/css" />
		<!-- Custom Stylesheet -->
		<link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/bootstrap-fileinput/css/fileinput.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ url('assets/bootstrap-fileinput/themes/explorer-fas/theme.css') }}" rel="stylesheet" type="text/css" />
        <!--end::Layout Skins -->
        <!-- begin::Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ url('assets/media/logos/favicon.ico') }}" />
		<!-- end::Icons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
	{{-- With Loader --}}
	<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">
        <!-- begin::Page loader -->
        <div class="kt-page-loader kt-page-loader--logo">
			{{-- <img alt="Logo" src="{{ url('assets/media/logos/logo-mini-md.png') }}"/>
			<div class="kt-loader--title">
				<h4 class="mt-3">DPMD PROVINSI BALI</h4>
			</div> --}}
            <div class="kt-spinner kt-spinner--danger"></div>
        </div>
		<!-- end::Page Loader -->
	{{-- Without Loader --}}
	{{-- <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading"> --}}
		<!-- begin:: Page -->
		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="{{ route('dashboard') }}">
					{{-- <img alt="Logo" src="{{ url('assets/media/logos/logo-light.png') }}" /> --}}
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>
		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

          		@include('partials.sidebar')

				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

						@include('partials.header')

                        <!-- content -->
                        @yield('content')

						@include('partials.footer')

				</div>
			</div>
		</div>
		<!-- end:: Page -->
		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<!-- end::Scrolltop -->
		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>
		<!-- end::Global Config -->
		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{ url('assets/vendors/global/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/scripts.bundle.js') }}" type="text/javascript"></script>
		<!--end::Global Theme Bundle -->
		<!--begin::Page Vendors(used by this page) -->
		<script src="{{ url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/vendors/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>
		<!--end::Page Vendors -->
		<!--begin::Page Scripts(used by this page) -->
		<script src="{{ url('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/pages/dashboard.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/pages/crud/forms/widgets/select2.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/plugins/piexif.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/plugins/sortable.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/fileinput.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/js/locales/id.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/themes/fas/theme.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/bootstrap-fileinput/themes/explorer-fas/theme.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/pages/crud/forms/validation/form-controls.js') }}" type="text/javascript"></script>
		<script src="{{ url('assets/js/demo1/pages/crud/file-upload/ktavatar.js') }}" type="text/javascript"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		{!! Toastr::message() !!}
		<!--end::Page Scripts -->
		<script src="{{ url('assets/js/as/app.js') }}" type="text/javascript"></script>

        @yield('scripts')

	</body>
	<!-- end::Body -->
</html>