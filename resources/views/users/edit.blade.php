@extends('layouts.app')

@section('page-title')
    Ubah Pengguna
@endsection

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Ubah Pengguna</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <span class="kt-subheader__desc">Ubah Pengguna</span>
                </div>
            </div>
        </div>
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label wrapper-back__button">
                                <div class="col-md-4 back-button mr-3">
                                    <a class="btn btn-primary" href="{{ route('users.index') }}"><i class="fa fa-angle-left"></i> Kembali</a>
                                </div>
                                <div class="col-md-8">
                                    <h3 class="kt-portlet__head-title">
                                        Ubah Pengguna
                                    </h3>
                                </div>
                            </div>
                        </div>

                        @include('partials.messages')
                        
                        <!--begin::Form-->
                        {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
                            <div class="kt-portlet__body">
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Nama *</label>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        {!! Form::text('name', null, array('placeholder' => 'Masukkan Nama','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Username *</label>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="input-group">
                                            {!! Form::text('username', null, array('placeholder' => 'Masukkan Username','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Email *</label>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="input-group">
                                            {!! Form::text('email', null, array('placeholder' => 'Masukkan Alamat Email','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Password *</label>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="input-group">
                                            {!! Form::password('password', array('placeholder' => 'Masukkan Password','class' => 'form-control', 'id' => 'password')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Konfirmasi Password *</label>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="input-group">
                                            {!! Form::password('confirm', array('placeholder' => 'Masukkan Konfirmasi Password','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Role</label>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="input-group">
                                            {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-success pull-right">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
@endsection