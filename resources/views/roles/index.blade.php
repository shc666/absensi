@extends('layouts.app')

@section('page-title')
    Pengaturan Roles
@endsection

@section('content')
  <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Content Head -->
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
      <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
          <h3 class="kt-subheader__title">Roles</h3>
          <span class="kt-subheader__separator kt-subheader__separator--v"></span>
          <span class="kt-subheader__desc">Pengaturan Roles</span>
        </div>
      </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
      <div class="row">
        <div class="col-lg-12">

          @include('partials.messages')
          
          <!--begin::Portlet-->
          <div class="kt-portlet">
            <div class="kt-portlet__head">
              <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                  <i class="flaticon2-lock"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                  Pengaturan Roles
                </h3>
              </div>
              <div class="kt-portlet__head-toolbar">
                @can('role-create')
                  <div class="kt-portlet__head-actions">
                    <a class="btn btn-success" href="{{ route('roles.create') }}"> Buat Role Baru</a>
                  </div>
                @endcan
              </div>
            </div>
            <div class="kt-portlet__body">
              <!--begin::Section-->
              <div class="kt-section">
                <div class="kt-section__content">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                            <th>No</th>
                            <th>Name Role</th>
                            <th width="280px">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($roles as $key => $role)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $role->name }}</td>
                            <td>
                                @can('role-list')
                                    <button type="button" class="btn btn-info btn-sm" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Lihat" data-original-title="Dark skin" onclick="location.href='{{ route('roles.show', $role->id) }}'">
                                      <i class="flaticon-eye"></i>
                                    </button>
                                @endcan
                                @can('role-edit')
                                    <button type="button" class="btn btn-warning btn-sm" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Ubah" data-original-title="Dark skin" onclick="location.href='{{ route('roles.edit', $role->id) }}'">
                                      <i class="flaticon2-edit"></i>
                                    </button>
                                @endcan
                                @can('role-delete')
                                  @if($role->removable)
                                    <button type="button" class="btn btn-danger btn-sm" data-skin="dark" data-toggle="kt-tooltip" data-placement="top" title="Hapus" data-original-title="Dark skin" onclick="Delete.data('{{ $role->id }}')">
                                      <i class="flaticon-delete"></i>
                                    </button>
                                  @endif
                                @endcan
                            </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>

                    {!! $roles->render() !!}
                    
                  </div>
                </div>
              </div>
              <!--end::Section-->
            </div>
          </div>
          <!--end::Portlet-->
        </div>
      </div>
    </div>
    <!-- end:: Content -->
  </div>
@endsection

@section('scripts')
  <script>
    var Delete = {
      "data" : function(rowid){
              swal.fire({
                title: 'Apakah anda yakin akan menghapus ini?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal!',
                reverseButtons: true
              }).then(function(result){
                if (result.value) {
                  $.ajax({
                        url: "roles/" + rowid,
                        type: 'DELETE',
                        data: {
                            '_token': '{{ csrf_token() }}'
                            },
                        dataType: "json",
                        success: function(data){
                          swal.fire(
                              'Sukses',
                              'Data telah berhasil terhapus!.',
                              'success'
                          ),
                            setTimeout(function() {
                              window.location.reload();
                          }, 2000);
                        }
                     });
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Batal',
                        'Gagal menghapus data!',
                        'error'
                    )
                }
            });
          },
      };
  </script>
@endsection