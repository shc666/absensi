"use strict";
var KTDatatablesDataSourceAjaxServer = function() {

	var initTable1 = function() {
		var table = $('#kt_table_1');

		// begin first table
		table.DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			ajax: 'http://localhost:8000/arsip/lebih-dari/tb-lebih-dari',
			columns: [
				{data: 'DT_RowIndex', type:"text", filter: false},
				{data: 'nama_arsip'},
				{data: 'nama_kategori'},
				{data: 'no_perjanjian'},
				{data: 'tgl_entry'},
                {data: 'status_id'},
                {data: 'files'},
				{data: 'id', responsivePriority: -1},
			],
			language: {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
			columnDefs: [
				{
					targets: -1,
					title: 'Aksi',
					orderable: false,
					render: function(data, type, full, meta) {
						return '\
                            <a href="http://localhost:8000/arsip/lebih-dari/'+data+'/ubah" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Ubah Data">\
								<i class="la la-edit"></i>\
							</a>\
							<a onclick="ConfirmDelete('+data+');" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Hapus Data">\
								<i class="la la-trash-o"></i>\
							</a>\
						';
					},
                },
                {
                    targets: -2,
                    title: 'Berkas Files',
                    orderable: false,
					render: function(data, type, full, meta) {
                        return '\
                            <a target="_blank" href="http://localhost:8000/upload/'+data+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Lihat File">\
                                <i class="la la-files-o"></i>\
                            </a>\
                        ';
					},
				},
				{
                    targets: -3,
                    orderable: false,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Belum Diverifikasi', 'class': 'kt-badge--info'},
							2: {'title': 'Diverifikasi', 'class': ' kt-badge--success'},
							3: {'title': 'Ditolak', 'class': ' kt-badge--danger'},
							4: {'title': 'Belum Layak', 'class': ' kt-badge--warning'}
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
					},
                },
			],
		});
	};
	

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

var ConfirmDelete = function(row_id){
    swal.fire({
        title: 'Anda yakin akan menghapus data ini?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		});
        $.ajax({
                url: "http://localhost:8000/arsip/lebih-dari/" +row_id,
                type: 'DELETE',
                dataType: "json",
                success: function(){
                swal.fire(
                    'Sukses',
                    'Data telah berhasil terhapus!.',
                    'success'
                ),
                    setTimeout(function() {
                    window.location.reload();
                }, 2000);
                }
            });
        }
        else if (result.dismiss === 'cancel') {
            swal.fire(
                'Batal',
                'Gagal menghapus data!',
                'error'
            )
        }
    });
};

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxServer.init();
});