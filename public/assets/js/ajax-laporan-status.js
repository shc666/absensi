'use strict';
var KTDatatablesDataSourceAjaxServer = function() {

	var initTable1 = function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		var table = $('#kt_table_1');

		// begin first table
		table.DataTable({
			destroy: true,
			responsive: true,
			searchDelay: 500,
			processing: true,
            serverSide: true,
            scrollX: true,
			ajax: {
				url: 'http://localhost:8000/laporan-status/dtb-status',
				type: 'GET',
				data: function(d) {
                    d.select_kerjasama = $('#kt_select2_1-13').val();
                    d.select_kategori = $('#kt_select2_1-14').val();
					d.select_status = $('#kt_select2_1-15').val();
					d.select_kab = $('#kt_select2_1-6').val();
					d.select_kec = $('#kt_select2_1-7').val();
					d.select_desa = $('#kt_select2_1-8').val();
                }
			},
			columns: [
				{data: 'DT_RowIndex', type:"text", filter: false},
				{data: 'nama_kabupaten'},
				{data: 'nama_kecamatan'},
				{data: 'nama_desa'},
				{data: 'nama_arsip'},
				{data: 'nama_kategori'},
				{data: 'status', width: '15%'},
				{data: 'jumlah', width: '5%'}
			],
			language: {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
			columnDefs: [
				{
					targets: -2,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Belum Diverifikasi', 'class': 'kt-badge--info'},
							2: {'title': 'Diverifikasi', 'class': ' kt-badge--success'},
							3: {'title': 'Ditolak', 'class': ' kt-badge--danger'},
							4: {'title': 'Belum Layak', 'class': ' kt-badge--warning'}
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
					},
				},
			]
		});
	};

	$('#submit').click(function (){
		$('#kt_table_1').DataTable().draw(true);
	});

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},
	};

}();

function reset_button()
{
	var table = $('#kt_table_1').DataTable();
	$(".kt-select2").val(null).trigger('change');
	table.ajax.reload();
}

jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxServer.init();
});