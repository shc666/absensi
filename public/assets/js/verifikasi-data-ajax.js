"use strict";
// Class definition

var KTDefaultDatatableDemo = function () {
	// Private functions

	// basic demo
	var demo = function () {

		var datatable = $('.kt-datatable').KTDatatable({
			data: {
				type: 'remote',
				source: {
					read: {
						url: 'http://localhost:8000/verifikasi-arsip/tb-arsip',
						method: 'GET',
						// sample custom headers
						headers: {'x-my-custokt-header': 'some value', 'x-test-header': 'the value'},
						map: function(raw) {
							// sample data mapping
							var dataSet = raw;
							if (typeof raw.data !== 'undefined') {
								dataSet = raw.data;
							}
							return dataSet;
						},
					}
				},
				pageSize: 10,
				serverPaging: true,
				serverFiltering: true,
				serverSorting: true
			},

			layout: {
				scroll: true,
				height: 550,
				footer: false
			},

			sortable: true,

			filterable: false,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			columns: [
				{
					field: 'no',
					title: 'No',
					sortable: false,
					width: 20,
					textAlign: 'center',
				}, {
					field: "nama_arsip",
					title: "Jenis Kerjasama",
					width: 'auto',
					sortable: false,
					autoHide: false,
				}, {
					field: "nama_desa",
					title: "Desa Pengusul",
					width: 'auto',
					sortable: false,
					autoHide: false,
				}, {
					field: "no_perjanjian",
					title: "No Perjanjian",
					width: 'auto',
					sortable: false,
					autoHide: false,
				}, {
					field: "nama_kategori",
					title: "Kategori Kerjasama",
					width: 'auto',
					sortable: false,
					autoHide: false,
				}, {
					field: 'tgl_entry',
					title: 'Tanggal Pengajuan',
					type: 'date',
					format: 'MM/DD/YYYY',
					sortable: false,
				}, {
					field: 'status',
					title: 'Status',
					sortable: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Belum Diverifikasi', 'class': 'kt-badge--info'},
							2: {'title': 'Diverifikasi', 'class': ' kt-badge--success'},
							3: {'title': 'Ditolak', 'class': ' kt-badge--danger'},
							4: {'title': 'Belum Layak', 'class': ' kt-badge--warning'}
						};
						return '<span class="kt-badge ' + status[row.status].class + ' kt-badge--inline kt-badge--pill">' + status[row.status].title + '</span>';
					},
				}, {
					field: 'Actions',
					title: 'Aksi',
					sortable: false,
					width: 50,
					overflow: 'visible',
					autoHide: false,
					template: function(row) {
						return '\
							<a href="http://localhost:8000/verifikasi-arsip/'+row.slug+'/lihat-arsip" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit details">\
								<i class="flaticon2-paper"></i>\
							</a>\
						';
					},
				}],

		});

    $('#kt_form_status').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#kt_form_status, #kt_form_type').selectpicker();

  };

	return {
		// public functions
		init: function () {
			demo();
		}
	};
}();

jQuery(document).ready(function () {
	KTDefaultDatatableDemo.init();
});