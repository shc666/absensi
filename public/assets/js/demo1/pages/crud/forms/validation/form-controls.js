// Class definition

var KTFormControls = function () {
    // Private functions
    
    var demo1 = function () {
        $( "#kt_form_1" ).validate({
            // define validation rules
            rules: {
                email: {
                    required: true,
                    email: true,
                    minlength: 10 
                },
                name: {
                    required: true 
                },
                username: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 8
                },
                password: {
                    required: true,
                    minlength: 8
                },
                confirm: {
                    required: true,
                    minlength: 8,
                    equalTo: "#password"
                },
                roles: {
                    required: true
                },
                permission: {
                    required: true
                },
                display_name: {
                    required: true
                },
                description: {
                    required: true
                },
                no_urut: {
                    required: true,
                    digits:true
                },
                nama: {
                    required: true
                },
                jenis_kelamin: {
                    required: true
                },
                tempat_lahir: {
                    required: true
                },
                tgl_lahir: {
                    required: true
                },
                nip: {
                    required: true,
                    digits: true
                },
                select_month: {
                    required: true,
                },
                select_year: {
                    required: true,
                },
                select_opd: {
                    required: true,
                },
                select_employee: {
                    required: true,
                },
                checkbox: {
                    required: true
                },
                options: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                },
                memo: {
                    required: true,
                    minlength: 10,
                    maxlength: 100
                },
                phone: {
                    required: true,
                    phoneUS: true 
                },
                checkbox: {
                    required: true
                },
                checkboxes: {
                    required: true,
                    minlength: 1,
                    maxlength: 2
                },
                radio: {
                    required: true
                }
            },
            messages: {
                name: "Bagian nama dibutuhkan.",
                email: "Bagian email dibutuhkan.",
                username: "Bagian username dibutuhkan.",
                password: {
                    required: "Bagian password dibutuhkan.",
                    minlength: "Bagian password minimal 8 karakter"
                },
                confirm: {
                    required: "Bagian konfirmasi password dibutuhkan.",
                    minlength: "Bagian konfirmasi password minimal 8 karakter",
                    equalTo: "Bagian konfirmasi password harus sama dengan password"
                },
                roles: "Bagian roles dibutuhkan.",
                permission: "Bagian permission dibutuhkan.",
                display_name: "Bagian tampilan permission dibutuhkan.",
                description: "Bagian deskripsi dibutuhkan.",
                no_urut: "Bagian nomor urut dibutuhkan dan hanya format angka.",
                nama: "Bagian nama dibutuhkan.",
                jenis_kelamin: "Bagian jenis kelamin dibutuhkan.",
                tempat_lahir: "Bagian tempat lahir dibutuhkan.",
                tgl_lahir: "Bagian tanggal lahir dibutuhkan.",
                nip: "Bagian nip dibutuhkan dan hanya format angka.",
                select_month: "Bagian pilihan bulan dibutuhkan",
                select_year: "Bagian pilihan tahun dibutuhkan",
                select_opd: "Bagian pilihan instansi dibutuhkan",
                select_employee: "Bagian pilihan pegawai dibutuhkan",
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#kt_form_1_msg');
                alert.removeClass('kt--hide').show();
                KTUtil.scrollTop();
                swal.fire({
                    "title": "", 
                    "text": "Masih terdapat bagian yang belum terisi. Mohon diperiksa kembali.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-primary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });
            },

            // submitHandler: function (form) {
            //     //form[0].submit(); // submit the form
            // }
        });       
    }

    var demo2 = function () {
        $( "#kt_form_2" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information
                billing_card_name: {
                    required: true
                },
                billing_card_number: {
                    required: true,
                    creditcard: true
                },
                billing_card_exp_month: {
                    required: true
                },
                billing_card_exp_year: {
                    required: true
                },
                billing_card_cvv: {
                    required: true,
                    minlength: 2,
                    maxlength: 3
                },

                // Billing Address
                billing_address_1: {
                    required: true
                },
                billing_address_2: {
                    
                },
                billing_city: {
                    required: true
                },
                billing_state: {
                    required: true
                },
                billing_zip: {
                    required: true,
                    number: true
                },

                billing_delivery: {
                    required: true
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {
                swal.fire({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
                swal.fire({
                    "title": "", 
                    "text": "Form validation passed. All good!", 
                    "type": "success",
                    "confirmButtonClass": "btn btn-secondary"
                });

                return false;
            }
        });       
    }

    return {
        // public functions
        init: function() {
            demo1(); 
            demo2();
        }
    };
}();

jQuery(document).ready(function() {    
    KTFormControls.init();
});